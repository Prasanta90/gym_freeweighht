import { Pipe, PipeTransform } from '@angular/core';

/**
 * Generated class for the TextLimitPipe pipe.
 *
 * See https://angular.io/api/core/Pipe for more info on Angular Pipes.
 */
@Pipe({
  name: 'textLimit',
})
export class TextLimitPipe implements PipeTransform {
  /**
   * Takes a value and makes it lowercase.
   */
  transform(value: string, ...args) {
    if(!value)
      return null;
    else if(value.length < 20)
      return value;
    else
    return value.substr(0, 20) +  '...';
    //return value.toLowerCase();
  }
}
