export class Notification{
    customerId: number;
    notificationId: number;
    notificationTitle: string;
    notificationBody: string;
    notificationDate: string;
    customerNotificationId: number;
}