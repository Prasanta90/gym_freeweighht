import { MstService } from './mst-service';
export class Subscription {
    subscriptionId: number;
    discount?: any;
    endDate: string;
    finalCost: number;
    packageCost: number;
    sendEmailFlag: string;
    sendSmsFlag: string;
    startDate: string;
    subscriptionFlag: string;
    subscrtionStatus: string;
    mstService: MstService;
}