export interface CalenderEventArrayModel {
    startDate?: any,
    endDate?: any,
}
export interface MyCalenderResponseModel{
    status?:string,
    eventList?:CalenderEventArrayModel[]
}