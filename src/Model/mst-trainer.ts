
export class  MstTrainerModel{
    trainerId: number;
    gymId: number;
    addrLine1: string;
    addrLine2: string;
    cityNm: string;
    contactNo: string;
    countryNm: string;
    dateOfBirth: string;
    emailId: string;
    fees: number;
    firstNm: string;
    gender: string;
    lastNm: string;
    pin_code: string;
    startDate: string;
    stateNm: string;
    stopDate: string;
    trainerImage?: any;
    trainerStatus: string;
}
