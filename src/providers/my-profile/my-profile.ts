import { HttpClient } from '@angular/common/http';
import { Http, RequestOptions, Headers } from "@angular/http";
import { Injectable } from '@angular/core';
import { CommonResponse } from "../../Model/common-response";
import { CommonProvider } from "../common/common";
import { Myprofile} from '../../Model/my-profile';

/*
  Generated class for the MyProfileProvider provider.

  See https://angular.io/guide/dependency-injection for more info on providers
  and Angular DI.
*/
@Injectable()
export class MyProfileProvider {

  public editprofileUrl: string = "/editprofile";
  public editprofileimageUrl: string = "/editprofileimage";
 // public getpaymentduedetailsUrl: string = "/getpaymentduedetails";
  public getpaymentdetailsUrl: string = "/getpaymentdetails";
  public getpackagedetailsUrl: string = "/getpackagedetails";
  public buypackageUlr: string = "/buypackage";
  

  constructor(public http: Http, public commonProvider: CommonProvider) {
    console.log('Hello MyProfileProvider Provider');
  }

  //edit profile
  editMyProfile(user: Myprofile) {
    
    console.log("editable values:::::"+JSON.stringify(user));
    let headers = new Headers({ 'Content-Type': 'application/json' });
    let options = new RequestOptions({ headers: headers });
    return this.http.post(this.commonProvider.hostUrl+this.editprofileUrl, user, options).toPromise()
        .then(res => <CommonResponse>res.json());
}

//edit customer image
editCustomerImage(username, customerImage){

  var imageInfo: any;
  imageInfo = {
    "username": username,
    "customerImage" : customerImage
  };
    
  console.log("editable values:::::"+JSON.stringify(imageInfo));
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
  return this.http.post(this.commonProvider.hostUrl+this.editprofileimageUrl, imageInfo, options).toPromise()
      .then(res => <CommonResponse>res.json());
}

//get Payment Due Details
getPaymentDueOrPaidDetails(username){

  var user: any;
  user = {
    "username": username
  };
    
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
  return this.http.post(this.commonProvider.hostUrl+this.getpaymentdetailsUrl, user, options).toPromise()
      .then(res => <CommonResponse>res.json());
}

//get my package details
getMyPackageDetails(serviceType: string, username: string){
  var pkg: any;
  pkg = {
    "serviceType": serviceType,
    "username": username
  };
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
  return this.http.post(this.commonProvider.hostUrl+this.getpackagedetailsUrl, pkg, options).toPromise()
      .then(res => <CommonResponse>res.json());
}

//post payment details
postPaymentetails(paymentPackageObj){
  
  let headers = new Headers({ 'Content-Type': 'application/json' });
  let options = new RequestOptions({ headers: headers });
  return this.http.post(this.commonProvider.hostUrl+this.buypackageUlr, paymentPackageObj, options).toPromise()
      .then(res => <CommonResponse>res.json());
}

}
