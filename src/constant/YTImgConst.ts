export const StandardDefinitionImage = "/sddefault.jpg";
export const MediumQualityImage = "/mqdefault.jpg";
export const HighQualityImage = "/hqdefault.jpg";
export const MaxResolutionImage = "/maxresdefault.jpg";