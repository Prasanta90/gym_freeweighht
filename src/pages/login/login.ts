import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, LoadingController, MenuController, Events, Platform  } from 'ionic-angular';
import { HometabsPage } from '../hometabs/hometabs';
import { ForgotPasswordPage } from '../../pages/forgot-password/forgot-password';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { Storage } from '@ionic/storage';
import { CommonProvider } from '../../providers/common/common';
import { LoginProvider } from '../../providers/login/login';
import { CommonResponse } from '../../Model/common-response';
import { Myprofile } from '../../Model/my-profile';
import { LoaderPage } from '../loader/loader';
import { Device } from '@ionic-native/device/ngx';
import { FcmService } from '../../fcm.service';


//@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  username: string;
  password: string;
  isRememberMe: boolean = false;
  signUpForm: FormGroup;
  loader: any;
  boolOneTimeEntry: boolean;
  response: CommonResponse;
  user: Myprofile;
  //customerName: string;
  customerImage: string;
  fcmToken: string;
  deviceTypeName: string;
  isDeviceTypeAndroid: string;
  isDeviceTypeIos: string;

  noPattern: string = "^(0|[1-9][0-9]*)$";
  //passwordPattern: string = "^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$";
  passwordPattern: string = "^(?=.*?[A-Z])(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{8,}$";
  emailPattern = "^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";

  validation_messages = {
    'username': [
      { type: 'required', message: 'You must enter your registered mobile number.' },
      { type: 'pattern', message: 'Please enter a valid mobile number.' },
      { type: "maxlength", message: "Phone number should be 10 digit number." },
      { type: "minlength", message: "Phone number should be 10 digit number." }
    ],

    'password': [
      { type: 'required', message: 'Please enter your password.' },
     { type: 'pattern', message: 'Password should contain at least one uppercase letter, one lowercase letter, one number and one special character.' },
     { type: "minlength", message: "Password should contain minimum 8 characters." },
      //{ type: "maxlength", message: "Password should contain maximum 10 characters." }
     // { type: 'pattern', message: 'Password should be composed of digits.' },
      //{ type: 'maxLength', message: 'Password should be 6 digit number.' }
    ]
  }

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder,
    public alertCtrl: AlertController, public commonProvider: CommonProvider, public plt: Platform,
    public activityLoader: LoadingController, public loginProvider: LoginProvider,
    public menu: MenuController, private device: Device, public events: Events, public fcm: FcmService) {


      
    this.signUpForm = this.formBuilder.group({
      'username': ['', [
        Validators.required,
        Validators.pattern(this.noPattern),
        Validators.maxLength(10),
        Validators.minLength(10)
      ]],
      //'password': ['', Validators.compose([Validators.required, Validators.pattern(this.noPattern), Validators.maxLength(6)])],
      'password': ['', [Validators.required, Validators.pattern(this.passwordPattern), Validators.minLength(8)]],
      'rememberMe': ['false']
    });

    
  }

  ionViewDidEnter() {
    this.fcm.getToken();

    

    //this.menu.swipeEnable(false);

    // If you have more than one side menu, use the id like below
    this.menu.swipeEnable(false, 'menu1');
  }

  ionViewWillEnter(){
    this.deviceTypeName = localStorage.getItem('deviceTypeName');
      this.isDeviceTypeAndroid = localStorage.getItem('isDeviceTypeAndroid');
      this.isDeviceTypeIos = localStorage.getItem('isDeviceTypeIos');
      console.log("this.deviceTypeName:::::"+this.deviceTypeName);
      console.log("this.isDeviceTypeAndroid:::::"+this.isDeviceTypeAndroid);
      console.log("this.isDeviceTypeIos:::::"+this.isDeviceTypeIos);

    this.loader = this.activityLoader.create({
      content: "Loading...",
      // duration: 3000,
      spinner: "ios",
      dismissOnPageChange: true //dismiss can be also used
    });
   
  }

  ionViewWillLeave() {
    // Don't forget to return the swipe to normal, otherwise 
    // the rest of the pages won't be able to swipe to open menu
    //this.menu.swipeEnable(true);

    // If you have more than one side menu, use the id like below
    this.menu.swipeEnable(true, 'menu1');
    
   
   }


  ionViewDidLoad() {
    console.log('ionViewDidLoad LoginPage');
    //Activity loader init
    

    this.boolOneTimeEntry = false;
  }

  // this.fcm.onNotifications().subscribe(data => {
  //   console.log(data);
  //   if (data.wasTapped) {
  //     console.log('Received in background');
  //     this.router.navigate([data.landing_page, data.price]);
  //   } else {
  //     console.log('Received in foreground');
  //     this.router.navigate([data.landing_page, data.price]);
  //   }
  // });

  //Function for Remember me
  isRemember(value) {
    console.log("value::::" + value);
  }

  /*login() {
    this.navCtrl.setRoot(HometabsPage, {animate: true, direction: 'forward'})
    // this.navCtrl.push(HomePage);
  }*/

  //Function for Forgot Password
  forgotpassword(data) {
    this.presentLoading();
    console.log("Forgot password value::::"+JSON.stringify(data));
    if (data.username != '' && data.username != null) {
      this.loginProvider.checkRegisteredEmail(data.username).then(res => {
        //this.presentLoading();
        this.response = res;
        console.log("Response for checking valid emailid:::" + JSON.stringify(this.response));
        if (this.response.status == 200 && this.response.message === 'success') {
          this.navCtrl.push(ForgotPasswordPage, { username: data.username, otp:  this.response.responseObj.OTP});
          //this.dismissLodaing();
        }
        else{
          this.showAlert("Alert", this.response.responseObj.forgotpassword);
          
        }
       
      })
      this.dismissLodaing();
        
    }
    else {
      this.showAlert("Alert", "Enter mobile number to proceed.");
      this.dismissLodaing();
    }

  }

  //Function for saving customer data
  saveUser(data) {
    console.log(this.fcmToken);
    this.fcmToken = localStorage.getItem("fcm_token");
    console.log("this.fcmToken>>>>"+this.fcmToken);
   
    // if(this.fcmToken){
    //   if (this.plt.is('android')) {
    //     // This will only print when on android
    //     console.log('I am an android device!');
    //     this.deviceType = 'Android';
    //     console.log('I am an android device!'+this.deviceType);
    //   }
    //   else if(this.plt.is('ios')) {
    //     this.deviceType = 'Ios';
    //   }
    // }

    if (this.plt.is('android')) {
      // This will only print when on android
      console.log('I am an android device!');
      this.deviceTypeName = 'Android';
      this.isDeviceTypeAndroid = 'true';
      this.isDeviceTypeIos = 'false';
      console.log('I am an android device!' + this.deviceTypeName);
    }
    else {
      this.deviceTypeName = 'Ios';
      this.isDeviceTypeAndroid = 'false';
      this.isDeviceTypeIos = 'true';
      console.log('I am an ios device!' + this.deviceTypeName);
    }
    localStorage.setItem('deviceTypeName', this.deviceTypeName);
    localStorage.setItem('isDeviceTypeAndroid', this.isDeviceTypeAndroid);
    localStorage.setItem('isDeviceTypeIos', this.isDeviceTypeAndroid);

   
    //localStorage.clear();
    
    console.log("data:::::" + JSON.stringify(data));
    if(data.username !== "" && data.password !== ""){
      this.presentLoading();
      this.loginProvider.login(data.username, data.password, this.fcmToken, this.deviceTypeName).then(res => {
        //this.presentLoading();
        this.response = res;
        console.log("Response:::" + JSON.stringify(this.response));
        if (this.response.status == 200) {
          this.user = this.response.responseObj.myprofile;
          console.log("After login: " + JSON.stringify(res));
          //console.log('Device UUID is:>>>> ' + this.device.uuid);
          if (data.rememberMe === true) {
            localStorage.setItem('rememberme', data.rememberMe);
          }
          if (this.user != null) {
            
            localStorage.setItem('username', this.user.username);
            localStorage.setItem('password', this.user.password);
            localStorage.setItem('customerId', this.user.customerId);
            localStorage.setItem('gymId', String(this.user.gymId));
           
            if (this.user.customerImage != null) {
              localStorage.setItem('customerImage', this.user.customerImage);
            }
            if (this.user.firstNm != null && this.user.lastNm != null) {
              // this.customerName = this.user.firstNm.concat(' ').concat(this.user.lastNm);
              localStorage.setItem('firstNm', this.user.firstNm);
              localStorage.setItem('lastNm', this.user.lastNm);
              //localStorage.setItem('customerName', this.customerName);
            }
  
            if (this.user.customerImage != null) {
              //this.customerImageUrl = "data:image/png;base64," + this.user.customerImage;
              this.customerImage = this.user.customerImage;
              localStorage.setItem('customerImage', this.customerImage);
            }
            if (this.user.gender != null) {
              localStorage.setItem('gender', this.user.gender);
            }
  
            console.log("User name:::::" + this.user.firstNm + " " + this.user.lastNm);
  
            localStorage.setItem('memberSince', this.user.createDate);
            this.dismissLodaing();
            this.navCtrl.setRoot(HometabsPage, { animate: true, direction: 'forward', user: this.user });
            this.events.publish('user:created',   this.user);
          }
        }
        else if (this.response.status == 400) {
          this.dismissLodaing();
          this.showAlert("Error", this.response.message)
  
  
        }
        // else {
        //   this.doConfirm();
        // }
      });

    }
    else{
      this.showAlert("Alert", "Please enter your mobile number and password.");
      //this.dismissLodaing();
    }

    


  }




  //Alert
  showAlert(title: string, msg: string) {
    const alert = this.alertCtrl.create({
      title: title,
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }

  //Activity Indicator
  presentLoading() {
    this.loader.present();
  }

  dismissLodaing() {
    this.loader.dismiss();
  }

  // doConfirm() {
  //   const confirm = this.alertCtrl.create({
  //     title: 'Login Failed',
  //     message: 'Please check your emailid or password.',
  //     buttons: [
  //       {
  //         text: 'Ok',
  //       }
  //     ]
  //   });
  //   confirm.present();
  // }

}
