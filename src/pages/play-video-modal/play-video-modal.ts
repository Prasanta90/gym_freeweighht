import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { DateWiseExerciseDetail } from '../../Model/date-wise-exercise-detail';
import { DomSanitizer } from '@angular/platform-browser';
import { YoutubeVideoPlayer } from '@ionic-native/youtube-video-player/ngx';

/**
 * Generated class for the PlayVideoModalPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-play-video-modal',
  templateUrl: 'play-video-modal.html',
})
export class PlayVideoModalPage {

  dateWiseExerciseDetailObj: DateWiseExerciseDetail = new DateWiseExerciseDetail();
  initialVLink = "https://www.youtube.com/embed/";
  videoLink: any;
  embedLink: any;
  iframe_html: any;
  youtubeId: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, private dom: DomSanitizer, 
    private youtube: YoutubeVideoPlayer, public viewCtrl: ViewController) {
    this.dateWiseExerciseDetailObj = this.navParams.get('dateWiseExerciseDetailObj');
    console.log("this.dateWiseExerciseDetailObj in modal****" + JSON.stringify(this.dateWiseExerciseDetailObj));
    var youtubeId = this.dateWiseExerciseDetailObj.videoLink.split("=");
    console.log(youtubeId[1]);
    this.youtubeId = youtubeId[1];

    this.videoLink = this.initialVLink + this.youtubeId;
    this.embedLink = this.dom.bypassSecurityTrustResourceUrl(this.videoLink);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PlayVideoModalPage');
  }

  openExerciseVideo() {
    this.youtube.openVideo(this.youtubeId);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }


}
