import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

/**
 * Generated class for the HealthInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-health-info',
  templateUrl: 'health-info.html',
})
export class HealthInfoPage {

  saveHealthInfo: FormGroup;
  weight: number = 90;
  hieght: string = '6ft';
  neck:number  = 3;
  shoulder: number = 8;
  bust: number = 7;
  chest: number = 34;
  abdominal: number = 10.5;
  hip: number = 34;
  waist: number = 3;
  leftBicep: number = 6;
  rightBicep: number = 6;
  leftTricep: number = 12.5;
  rightTricep: number = 34.6;
  leftArm: number = 10.25;
  rightArm: number = 12.35;
  leftThigh: number =  3;
  rightThigh: number = 4;
  leftCalf: number = 4;
  rightCalf: number = 5;

  constructor(public navCtrl: NavController, public navParams: NavParams, public formBuilder: FormBuilder) {
    this.saveHealthInfo = this.formBuilder.group({
      'weight': [this.weight],
      'hieght': [this.hieght],
      'neck': [this.neck],
      'shoulder': [this.shoulder],
      'bust': [this.bust],
      'chest': [this.chest],
      'abdominal': [this.abdominal],
      'hip': [this.hip],
      'waist': [this.waist],
      'leftBicep': [this.leftBicep],
      'rightBicep': [this.rightBicep],
      'leftTricep': [this.leftTricep],
      'rightTricep': [this.rightTricep],
      'leftArm': [this.leftArm],
      'rightArm': [this.rightArm],
      'leftThigh': [this.leftThigh],
      'rightThigh': [this.rightThigh],
      'leftCalf': [this.leftCalf],
      'rightCalf': [this.rightCalf]
    })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HealthInfoPage');
  }

  saveHealthInfoEvent(data){
    console.log(JSON.stringify(data));
  }

}
