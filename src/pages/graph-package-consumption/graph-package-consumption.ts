import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { Chart } from 'chart.js';

/**
 * Generated class for the GraphPackageConsumptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-graph-package-consumption',
  templateUrl: 'graph-package-consumption.html',
})
export class GraphPackageConsumptionPage {

  @ViewChild('pieChart') pieChart;
  public myProgress: any = {
    "myProgress": [
      {
        'monthName': 'Jan',
        'time': 15,
        'color': 'rgba(206, 61, 95, 0.5)',
        'hover': 'rgba(199, 108, 129, 0.5)'
      },
      {
        'monthName': 'Feb',
        'time': 17,
        'color': 'rgba(83, 131, 185, 0.5)',
        'hover': 'rgba(122, 160, 202, 0.5)'
      },
      {
        'monthName': 'Mar',
        'time': 12,
        'color': 'rgba(198, 147, 194, 0.5)',
        'hover': 'rgba(200, 166, 197, 0.5)'
      },
      {
        'monthName': 'Apr',
        'time': 20,
        'color': 'rgba(54, 116, 152, 0.5)',
        'hover': 'rgba(103, 139, 160, 0.5)'
      },
      {
        'monthName': 'May',
        'time': 3,
        'color': 'rgba(152, 54, 145, 0.5)',
        'hover': 'rgba(152, 89, 149, 0.5)',
      },
      {
        'monthName': 'Jun',
        'time': 9,
        'color': 'rgba(192, 192, 192, 0.5)',
        'hover': 'rgba(220, 220, 220, 0.5)'
      },
      {
        'monthName': 'Jul',
        'time': 10,
        'color': 'rgba(192, 192, 192, 0.5)',
        'hover': 'rgba(220, 220, 220, 0.5)'
      },
      {
        'monthName': 'Aug',
        'time': 6,
        'color': 'rgba(192, 192, 192, 0.5)',
        'hover': 'rgba(220, 220, 220, 0.5)'
      },
      {
        'monthName': 'Sept',
        'time': 20,
        'color': 'rgba(192, 192, 192, 0.5)',
        'hover': 'rgba(220, 220, 220, 0.5)'
      },
      {
        'monthName': 'Oct',
        'time': 17,
        'color': 'rgba(192, 192, 192, 0.5)',
        'hover': 'rgba(220, 220, 220, 0.5)'
      },
      {
        'monthName': 'Nov',
        'time': 10,
        'color': 'rgba(192, 192, 192, 0.5)',
        'hover': 'rgba(220, 220, 220, 0.5)'
      },
      {
        'monthName': 'Dec',
        'time': 10,
        'color': 'rgba(192, 192, 192, 0.5)',
        'hover': 'rgba(220, 220, 220, 0.5)'
      }
    ]
  };

  myProgess: string = "No. of days attend gym";
  //barChart: any;
  public pieChartEl: any;
  public barChartEl: any;
  public lineChartEl: any;
  public chartLabels: any = [];
  public chartValues: any = [];
  public chartColours: any = [];
  public chartHoverColours: any = [];
  public chartLoadingEl: any;

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
   this.defineChartData();
   this.createPieChart();
  }

  defineChartData(): void {
    let k: any;

    for (k in this.myProgress.myProgress) {
      var month = this.myProgress.myProgress[k];

      this.chartLabels.push(month.monthName);
      this.chartValues.push(month.time);
      this.chartColours.push(month.color);
      this.chartHoverColours.push(month.hover);
    }
  }

  createPieChart() {

    this.pieChartEl = new Chart(this.pieChart.nativeElement,
      {
        type: 'pie',
        data: {
          labels: this.chartLabels,
          datasets: [{
            label: 'Package Consumption',
            data: this.chartValues,
            duration: 2000,
            easing: 'easeInQuart',
            backgroundColor: this.chartColours,
            hoverBackgroundColor: this.chartHoverColours
          }]
        },
        options: {
          maintainAspectRatio: false,
          layout: {
            padding: {
              left: 50,
              right: 0,
              top: 0,
              bottom: 0
            }
          },
          animation: {
            duration: 5000
          }
        }
      });

    this.chartLoadingEl = this.pieChartEl.generateLegend();
  }


}
