import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';

/**
 * Generated class for the ClubWorkoutDescriptionPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-club-workout-description',
  templateUrl: 'club-workout-description.html',
})
export class ClubWorkoutDescriptionPage {

  topPicks: any;

  constructor(public navCtrl: NavController, public navParams: NavParams, public viewCtrl: ViewController) {
    this.topPicks = this.navParams.get('topPicks');
    console.log("this.topPicks in club-workout-exercise-description-modals****" + JSON.stringify(this.topPicks));
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad ClubWorkoutDescriptionPage');
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

}
