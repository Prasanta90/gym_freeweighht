import { Component } from '@angular/core';
import { IonicPage, NavController } from 'ionic-angular';
import { MyProfilePage } from '../my-profile/my-profile';
// import { InboxPage } from '../inbox/inbox';
// import { NotificationsPage } from '../notifications/notifications';
// import { HealthInfoPage } from '../health-info/health-info';
import { PaymentsDuesPage } from '../payments-dues/payments-dues';
import { TodaySWorkoutNewPage} from '../today-s-workout-new/today-s-workout-new';

/**
 * Generated class for the PaymentHistoryTabsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

//@IonicPage()
@Component({
  selector: 'page-payment-history-tabs',
  templateUrl: 'payment-history-tabs.html',
})
export class PaymentHistoryTabsPage {

  profileRoot = MyProfilePage
  paymentsDuesRoot = PaymentsDuesPage
  todaySWorkoutPage = TodaySWorkoutNewPage

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentHistoryTabsPage');
  }

}
